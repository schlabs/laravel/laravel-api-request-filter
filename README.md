
# laravel-api-request-filter
  

## Description

API filtering, sorting and pagination for Laravel
  

## Depencencies

* 
  

## Installation
  

**In your composer.json**

Add the following code to the "repositories" key:

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://gitlab.com/schlabs/laravel/laravel-api-request-filter"
	}
]
```

Add this line to your require dependecies:

```json
"schlabs/laravel-api-request-filter": "1.0.*"
```
 
**In your project root run:**
```sh
composer update
```

## Usage

Include the trait "SchLabs\ApiRequestFilter\Traits\ApiRequestFilter" in your Model class
