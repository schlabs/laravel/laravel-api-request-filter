<?php
namespace SchLabs\LaravelApiRequestFilter\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait ApiRequestFilter
{
    /**
     * Paginate data
     *
     * @param \Illuminate\Http\Request $request
     */
    public function scopePaginateData($query, $request)
    {
        if ($request->filled('paginate')) {
            if (!$request->boolean('paginate')) {
                return $query->get();
            }
        }

        $pages = $request->filled('perPage') ? $request->perPage : 15;
        return $query->paginate($pages);

    }

    /**
     * Sort data
     *
     * Sort data, can sort by multiple fields, separated with pipe (|) character.
     * If you need a custom order for a column, you should use the customOrder parameter
     *
     * @param \Illuminate\Http\Request $request
     * @param array $customOrder Custom order for a given column
     */
    public function scopeOrder($query, $request, array $customOrder = [])
    {
        return $query->when($request->filled('orderBy'), function ($query) use ($request, $customOrder) {

            $orders = explode(',', $request->orderBy);

            foreach ($orders as $order) {
                $orderAr = explode('|', $order);

                if (array_key_exists($orderAr[0], $customOrder)) {
                    $query->orderByRaw($customOrder[$orderAr[0]]);
                } else {
                    $query->orderBy($orderAr[0], $orderAr[1] ?? 'asc');

                }
            }
        });
    }


    /**
     * Supported operators
     * =  Equals to
     * !  Not equals to
     * null  Is null
     * !null  Is not null
     * ,  In
     * |  Between
     *
     * @param $value
     * @return string
     */
    private function operator($value)
    {
        $value = strtolower($value);

        //If null
        if ($value === 'null') {
            $operator = 'null';
        } //If not null
        else if ($value === '!null') {
            $operator = '!null';
        } //If multiple
        else if (str_contains($value, ',')) {
            $operator = 'in';
        } //If between
        else if (str_contains($value, '|')) {
            $operator = '|';
        } //If LIKE
        else if (str_contains($value, '!like:')) {
            $operator = '!%';
        } //If not
        else if (str_contains($value, 'like:')) {
            $operator = '%';
        }
        else if (!is_bool($value) && $value[0] === '!') {
            $operator = '!=';
        }
        else if (str_contains($value, 'json:')) {
            $operator = '@>';
        }
        else {
            $operator = '=';
        }

        return $operator;
    }

    /**
     * @param Builder $query
     * @param string $key
     * @param string $operator
     * @param mixed $value
     * @return Builder
     */
    private function whereGenerator(&$query, $key, $operator, $value, $isMorph = false, $morphType = null, $or = '', $table = '')
    {
        if(!empty($table)){
            $table = "$table.";
        }

        if ($isMorph) {
            $fillable = (new $morphType)->getFillable();

            if (!in_array($key, $fillable)) {
                return $query;
            }
        }

        $query = $this->addWhere($query, $value, $operator, $table . $key);
        $query->when(!empty($or), function ($query) use ($or, $operator, $key, $isMorph, $morphType){
            $query->orWhere(function($query) use ($or, $operator, $key){
                $query = $this->addWhere($query, $or,  $operator, $key);
            });
        });

        return $query;

    }

    private function addWhere($query, $value, $operator, $key)
    {
        switch ($operator) {
            case '=':
            case '!=':
                $value = str_replace('!', '', $value);
                $query->where($key, $operator, $value);
                break;
            case 'in':
                $value = explode(',', $value);
                $query->whereIn($key, $value);
                break;
            case 'null':
                $query->whereNull($key);
                break;
            case '!null':
                $query->whereNotNull($key);
                break;
            case '%':
                $value = preg_replace('/(like\:)/i', '', $value);
                $query->where($key, 'LIKE', '%' . $value . '%');
                break;
            case '!%':
                $value = preg_replace('/(!like\:)/i', '', $value);
                $query->where($key, 'NOT LIKE', '%' . $value . '%');
                break;
            case '|':
                $values = explode('|', $value);
                $query->whereBetween($key, $values);
                break;
            case '@>':
                $value = preg_replace('/(json\:)/i', '', $value);
                $query->whereRaw("$key @> '$value'");
                break;
        }

        return $query;
    }

    /**
     * Filter data
     *
     * The filter is based on the fillable property in the target model. If
     * the column to filter is not on the fillable property, the filter won't work and
     * should be added to the extra parameter.
     *
     * @param $request
     * @param array $extra Extra columns to filter
     * @return mixed
     */
    public function scopeFilter($query, $request, $extra = [])
    {
        $defaultExtraFilters = ['id'];
        $fillable = array_merge($this->fillable, $defaultExtraFilters, $extra);
        $filters = $request->all();
        $that = $this;
        $whereQuery = [];
        $whereRelations = [];

        collect(Arr::dot($filters))
            ->each(function ($value, $key) use ($fillable, &$whereQuery, &$whereRelations, $that) {
                $where = $this->whereNotation($key, $this, $value);

                //If field doenst exists, continue
                if (!$where) {
                    return true;
                }

                $keys = explode('->', $key);
                $keysCount = count($keys);
                $operator = $this->operator($where['isJson'] ? "json:" : $value);
                if ($where['isRelation']) {
                    $whereRelations[$where['relationPath']] = [
                        'key' => $where['notation'],
                        'operator' => $operator,
                        'value' => $where['value'],
                        'isMorph' => $where['isMorph'],
                        'or' => $where['jsonQuoted'],
                        'table' => $where['table']
                    ];

                } else {
                    $whereQuery[] = [
                        'key' => $where['notation'],
                        'operator' => $operator,
                        'value' => $where['value'],
                        'or' => $where['jsonQuoted']
                    ];
                }

            });

        //Where keys
        foreach ($whereQuery as $where) {
            /**
             * @var $operator
             * @var $key
             * @var $value
             */
            extract($where);
            $this->whereGenerator($query, $key, $operator, $value);
        }


        //Where relations
        foreach ($whereRelations as $relation => $queryRelation) {

            if ($queryRelation['isMorph']) {
                $query->whereHasMorph(
                    $relation,
                    '*',
                    function ($query, $type) use ($queryRelation) {
                        $this->whereGenerator(
                            $query,
                            $queryRelation['key'],
                            $queryRelation['operator'],
                            $queryRelation['value'],
                            $queryRelation['isMorph'],
                            $type,
                            $queryRelation['or'],
                        );
                    }
                );
            } else {
                $query->whereHas(
                    $relation,
                    function ($query) use ($queryRelation, $relation) {
                        $this->whereGenerator(
                            $query,
                            $queryRelation['key'],
                            $queryRelation['operator'],
                            $queryRelation['value'],
                            false,
                            null,
                            $queryRelation['or'],
                            $queryRelation['table']

                        );
                    }
                );
            }

        }

        return $query;

    }

    /**
     * Generate the where notation (where->andWhere->etc) for the filter
     * @param $notation
     * @param $class
     * @param $value
     * @param $relationPath
     * @return array
     */
    private function whereNotation($notation, $class, $value, $relationPath = '', $convertType = true)
    {
        $result = '';
        /** @var Model $obj */
        $obj = $class ? new $class : new $this;

        $fillable = array_merge($obj->getFillable(), ['id']);
        $route = explode('->', $notation);
        $nextRoute = array_slice($route, 1, count($route));
        $base = $route[0];
        $isArray = strpos($base, '*');
        $isRelation = false;
        $isJson = false;
        $isMorph = false;
        $exists = false;
        $jsonQuoted = [];
        $table = $obj->getTable();

        $base = str_replace('*', '', $base);

        //check if its a class property
        //ex: class->property
        if (in_array($base, $fillable)) {

            $exists = true;

            //check if its a json property
            //ex class->json-property->property
            if (count($route) > 1) {
                $isJson = true;
                //generate json query
                $json = [];
                foreach (array_reverse($nextRoute) as $key => $val) {
                    if ($key === 0) {
                        $json = [$val => $this->convertToType($value)];
                        $jsonQuoted = [$val => $value];
                    } else {
                        $json = [$val => $json];
                        $jsonQuoted = [$val => $jsonQuoted];
                    }
                }

                $json = json_encode($json);
                $jsonQuoted = json_encode($jsonQuoted);

                if ($isArray) {
                    $json = "[$json]";
                    $jsonQuoted = "[$jsonQuoted]";
                }

                $notation = $base;
                $value = $json;
            }

        } //ex: class->relation->property or class->relation->json-property->property
        else if (method_exists($obj, Str::camel($base))) {
            $relationName = Str::camel($base);
            $exists = true;

            $relationPath = $relationPath === '' ? $relationName : "$relationPath.$relationName";

            $isRelation = true;
            /** @var Model $relationClass */
            $relationClass = get_class($obj->{$relationName}()->getRelated());

            //When the relation is morph, getRelated returns the parent model
            if($relationClass !== get_class($obj)){

                $result = $this->whereNotation(implode('->', $nextRoute), $relationClass, $value, $relationPath, $convertType);

                if(is_array($result)){
                    $notation = $result['notation'];
                    $value = $result['value'];
                    $isJson = $result['isJson'];
                    $relationPath = $result['relationPath'];
                    $jsonQuoted = $result['jsonQuoted'];
                    $isMorph = strpos(strtolower(get_class($obj->{$relationName}())), 'morph') !== false;
                    $table = (new $relationClass)->getTable();
                }
            }else{
                $notation = implode('.', $nextRoute);
                $isMorph = true;
            }

        }

        if (!$exists) {
            return false;
        }

        return compact('notation', 'value', 'isRelation', 'isJson', 'isMorph', 'relationPath', 'jsonQuoted', 'table');
    }

    /**
     * Convert string to type
     * @param $value
     * @return mixed
     */
    private function convertToType($value)
    {
        if($value == 'true' || $value == 'false'){
            return filter_var($value, FILTER_VALIDATE_BOOLEAN);
        }else if(is_numeric($value)){
            return filter_var($value, FILTER_VALIDATE_INT);
        }

        return $value;
    }
}
